﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TiburonTest.Model.ApiClasses;
using TiburonTest.Model.DAL;

namespace TiburonTest.Controllers
{
    //Контроллер АПИ приложения
    public class QuestController : Controller
    {
        private IAnswersStorage _storage;

        public QuestController(IAnswersStorage storage)
        {
            _storage = storage;
        }
        
        [HttpPost]
        public async Task<ApiResponse> PostAnswers([FromBody]ApiRequest answer)
        {
            var response = new ApiResponse();
            //На всякий случай проверяем корректность пола, если что, возвращаем код 1
            if ((answer.gender != "male") && (answer.gender != "female")) response.errorCode = 1;
            else await _storage.AddAnswer(answer);
            return response;
        }
    }
}