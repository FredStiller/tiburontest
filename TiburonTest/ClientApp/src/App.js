﻿import React from 'react'
import './w3.css'

export default class App extends React.Component {
    constructor() {
        super();

        //Массив с марками телефонов и их обозначениями
        this.phones = [
            {
                value: 'Apple',
                label: 'Apple',
                checked: false
            },
            {
                value: 'Samsung',
                label: 'Samsung',
                checked: false
            },
            {
                value: 'Xiaomi',
                label: 'Xiaomi',
                checked: false
            },
            {
                value: 'Nokia',
                label: 'Nokia',
                checked: false
            },
            {
                value: 'Other',
                label: 'Другое',
                checked: false
            }
        ];

        //Здесь храним пол пользователя
        this.gender = 'male';

        //Состояния приложения
        //showRedString - если пользователь не выбрал ни одной марки, показываем красную строку
        //screen - общее состояние страницы. Их всего три:
        //init - начальное состояние с вопросами
        //success - ответы приняты системой и занесены в БД, всё ОК
        //error - на сервере случилась ошибка
        this.state = {
            showRedString: 'none',
            screen: 'init'
        };


        //Биндим методы
        this.onGenderChangedHandler = this.onGenderChangedHandler.bind(this);
        this.onPhoneChangeHandler = this.onPhoneChangeHandler.bind(this);
        this.generateCheckbox = this.generateCheckbox.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    //Обработчик радиокнопок с полами
    onGenderChangedHandler(event) {
        this.gender = event.currentTarget.value;
    }


    //Обработчик чекбоков с марками телефонов
    //Находим выбранный(или не выбранный) телефон в массиве и меняем его checked на противоположный
    onPhoneChangeHandler(event) {
        let phone = this.phones.find((element) => (event.target.value === element.value));
        phone.checked = !phone.checked;
    }

    //Обработчик нажатия кнопки "Отправить"
    async onSubmit() {
        //Аггрегируем данные в один объект
        let submitData = {
            gender : this.gender,
            phones: this.phones
                .filter((element) => element.checked)
                .map((element) => element.value)
        }
        //Проверяем, что хотя бы одна марка выбрана, если нет, показываем красную строку
        if (submitData.phones.length <= 0) this.setState({
            showRedString: 'block'
        });
        else {
            //Отправляем собранные данные на сервер
            let response = await fetch('/api/Quest/PostAnswers', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(submitData)
            });
            //Проверяем код ответа и его содержимое, выставляем соответствующий screen
            if (response.ok) {
                let responseBody = await response.json();
                if (responseBody !== undefined && responseBody.errorCode === 0)
                    this.setState({
                        screen: 'success'
                    });
                else this.setState({
                        screen: 'error'
                    });
            }
            else this.setState({
                    screen: 'error'
                });
        }
    }

    //Простой генератор чекбоксов
    generateCheckbox(descr) {
        return (
            <div>
                <input className="w3-check" type="checkbox" name="phone" value={descr.value} onChange={this.onPhoneChangeHandler}></input>
                <label>{descr.label}</label>
            </div>)
    }

    render() {
        if (this.state.screen === 'init')
            return (
                <div class="w3-card w3-white w3-padding-large w3-margin-top" style={{ width: 300 + 'px', margin: 'auto' }}>
                    <h6>Укажите ваш пол</h6>
                    <div id="gender-choice">
                        <input class="w3-radio" type="radio" name="gender" value="male" onClick={this.onGenderChangedHandler} checked></input>
                        <label>Мужской</label>
                        <br />
                        <input class="w3-radio" type="radio" name="gender" value="female" onClick={this.onGenderChangedHandler}></input>
                        <label>Женский</label>
                    </div>
                    <h6>Марка вашего телефона</h6>
                    <div id="phone-choice">
                        {this.phones.map(this.generateCheckbox)}
                    </div>
                    <div class="w3-center w3-margin-top">
                        <div class="w3-button w3-blue" style={{ width: 9 + 'em', margin: 'auto' }} onClick={this.onSubmit}>Отправить</div>
                        <p />
                        <div class="w3-text-red" style={{ display: this.state.showRedString }}>Укажите хотя бы один телефон</div>
                    </div>
                </div>);
        else if (this.state.screen === 'success')
            return (
                <div className="w3-center">
                    <h4 className="w3-margin-top">Спасибо за ответы</h4>
                </div>);
        else if (this.state.screen === 'error')
            return (
                <div className="w3-center">
                    <h4 className="w3-margin-top">Что-то пошло не так. Пожалуйста, повторите попытку позднее</h4>
                </div>);
    }
};