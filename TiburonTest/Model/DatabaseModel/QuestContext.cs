﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TiburonTest.Model.DatabaseModel
{
    public class QuestContext : DbContext
    {
        public QuestContext(DbContextOptions<QuestContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Делаем seed для телефонов
            modelBuilder.Entity<Phone>().HasData(
                    new Phone{ Id = 1, Name = "Apple" },
                    new Phone{ Id = 2, Name = "Samsung" },
                    new Phone{ Id = 3, Name = "Xiaomi" },
                    new Phone{ Id = 4, Name = "Nokia" },
                    new Phone{ Id = 5, Name = "Other" });

            //Настраиваем many-to-many соотношение между телефонами и пользователями
            modelBuilder.Entity<UsersPhones>()
                .HasOne(up => up.Phone)
                .WithMany(p => p.UsersPhones)
                .IsRequired();

            modelBuilder.Entity<UsersPhones>()
                .HasOne(up => up.User)
                .WithMany(u => u.UsersPhones)
                .IsRequired();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UsersPhones> UserPhones { get; set; }
        public DbSet<Phone> Phones { get; set; }
    }
}
