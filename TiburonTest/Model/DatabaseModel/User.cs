﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TiburonTest.Model.DatabaseModel
{
    public class User
    {
        public int Id { get; set; }
        //Дополнительная проверка поступивших данных о поле юзера
        [MaxLength(6)]
        public string Gender { get; set; }

        public ICollection<UsersPhones> UsersPhones { get; set; }
    }
}
