﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TiburonTest.Model.DatabaseModel
{
    public class Phone
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<UsersPhones> UsersPhones { get; set; }
    }
}
