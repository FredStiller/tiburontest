﻿using System.Threading.Tasks;
using TiburonTest.Model.ApiClasses;

namespace TiburonTest.Model.DAL
{
    //Интерфейс сервиса-хранилища ответов
    public interface IAnswersStorage
    {
        Task AddAnswer(ApiRequest request);
    }
}
