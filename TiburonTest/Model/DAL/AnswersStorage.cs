﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TiburonTest.Model.ApiClasses;
using TiburonTest.Model.DatabaseModel;

namespace TiburonTest.Model.DAL
{
    //Реализация сервиса=хранилища ответов
    public class AnswersStorage : IAnswersStorage
    {
        private QuestContext _context;

        public AnswersStorage(QuestContext context)
        {
            _context = context;
        }

        public async Task AddAnswer(ApiRequest answer)
        {
            
            //Создаём список с id марок телефонов
            var userPhones = _context.Phones
                .Where(p => answer.phones.Contains(p.Name))
                .Select(p => new UsersPhones { PhoneId = p.Id })
                .ToArray();

            //Проверяем, что среди пришедших марок телефонов есть хоть одна правильная
            //Если нет, бросаем исключение
            if (userPhones.Length <= 0) throw new Exception();

            var user = new User
            {
                Gender = answer.gender,
                UsersPhones = userPhones
            };
            await _context.Users.AddAsync(user);
            _context.SaveChanges();
        }
    }
}
