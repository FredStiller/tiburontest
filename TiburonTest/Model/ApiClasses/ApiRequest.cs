﻿namespace TiburonTest.Model.ApiClasses
{
    //Класс для реквестов с клиента
    public class ApiRequest
    {
        public string gender { get; set; }
        public string[] phones { get; set; }
    }
}
